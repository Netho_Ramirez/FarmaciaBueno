﻿using System;
using Farmacia.COMMON.Entidades;
using System.Collections.Generic;
using System.Text;

namespace Farmacia.COMMON.Interfaces
{
    public interface IManejadorClientes : IManejadorGenerico<Clientes>
    {
        List<Clientes> ClientesPorNombre(string nombre);
    }
}
