﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Interfaces;
using LiteDB;

namespace Farmacia.DAL
{
    public class RepositorioDeVentas : IRepositorio<Ventasst>
    {
        private string DBName = "Inventario.db";
        private string TableName = "Ventas";
        public List<Ventasst> Read
        {
            get
            {
                List<Ventasst> datos = new List<Ventasst>();
                using (var db = new LiteDatabase(DBName))
                {
                    datos = db.GetCollection<Ventasst>(TableName).FindAll().ToList();
                }
                return datos;
            }
        }

        public bool Create(Ventasst entidad)
        {
            entidad.Id = Guid.NewGuid().ToString();
            try
            {
                using (var db = new LiteDatabase(DBName))
                {
                    var coleccion = db.GetCollection<Ventasst>(TableName);
                    coleccion.Insert(entidad);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(string id)
        {
            try
            {
                int r;
                using (var db = new LiteDatabase(DBName))
                {
                    var coleccion = db.GetCollection<Ventasst>(TableName);
                    r = coleccion.Delete(e => e.Id == id);
                }
                return r > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update(Ventasst entidadModificada)
        {
            try
            {
                using (var db = new LiteDatabase(DBName))
                {
                    var coleccion = db.GetCollection<Ventasst>(TableName);
                    coleccion.Update(entidadModificada);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
