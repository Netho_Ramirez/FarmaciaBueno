﻿using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Farmacia.BIZ
{
    public class ManejadorDeVentas : IManejadorVentas
    {
        IRepositorio<Ventasst> repositorio;
        public ManejadorDeVentas(IRepositorio<Ventasst> repositorio)
        {
            this.repositorio = repositorio;
        }
        public List<Ventasst> Listar => repositorio.Read;

        public bool Agregar(Ventasst entidad)
        {
            return repositorio.Create(entidad);
        }

        public Ventasst BuscarPorId(string id)
        {
            return Listar.Where(e => e.Id == id).SingleOrDefault();
        }

        public bool Eliminar(string id)
        {
            return repositorio.Delete(id);
        }

        public bool Modificar(Ventasst entidad)
        {
            return repositorio.Update(entidad);
        }
    }
}

