﻿using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Farmacia.BIZ
{
    public class ManejadorProductos : IManejadorProductos
    {
        IRepositorio<Productos> repositorio;
        public ManejadorProductos(IRepositorio<Productos> repo)
        {
            repositorio = repo;
        }

        public List<Productos> Listar => repositorio.Read;

        public bool Agregar(Productos entidad)
        {
            return repositorio.Create(entidad);
        }

        public Productos BuscarPorId(string id)
        {
            return Listar.Where(e => e.Id == id).SingleOrDefault();
        }

        public bool Eliminar(string id)
        {
            return repositorio.Delete(id);
        }

        public List<Productos> ProductosPorNombre(string nombre)
        {
            return Listar.Where(e => e.NombreProducto == nombre).ToList();
        }

        public bool Modificar(Productos entidad)
        {
            return repositorio.Update(entidad);
        }
    }
}
