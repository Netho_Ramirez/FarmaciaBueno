﻿using Farmacia.BIZ;
using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Interfaces;
using Farmacia.DAL;
using System;
using System.Windows;

namespace Farmacia.GUI.Administrador
{
    /// <summary>
    /// Lógica de interacción para AgregarEmpleados.xaml
    /// </summary>
    public partial class AgregarEmpleados : Window
    {
        enum accion
        {
            Nuevo,
            Editar
        }

        IManejadorEmpleados manejadorEmpleados;
        accion accionEmpleados;
        bool esNuevo;
        public AgregarEmpleados()
        {
            InitializeComponent();
            manejadorEmpleados = new ManejadorEmpleados(new RepositorioDeEmpleados());

            PonerBotonesEmpleadosEnEdicion(false);
            LimpiarCamposDeEmpleados();
            ActualizarTablaEmpleados();
            HabilitarCajas(false);
            HabilitarBotones(true);
        }

        private void HabilitarBotones(bool habilitados)
        {
            btnAgregarEmpleado.IsEnabled = habilitados;
            btnEliminarEmpleado.IsEnabled = habilitados;
            btnEditarEmpleado.IsEnabled = habilitados;
            btnGuardarEmpleado.IsEnabled = !habilitados;
            btnCancelar.IsEnabled = !habilitados;
        }

        private void HabilitarCajas(bool habilitadas)
        {
            txbNombreEmpleado.Clear();
            txbNombreEmpleado.IsEnabled = habilitadas;
            txbApellidoEmpleado.Clear();
            txbApellidoEmpleado.IsEnabled = habilitadas;
            txbPuestoEmpleado.Clear();
            txbPuestoEmpleado.IsEnabled = habilitadas;            
        }

        private void ActualizarTablaEmpleados()
        {
            dtgEmpleados.ItemsSource = null;
            dtgEmpleados.ItemsSource = manejadorEmpleados.Listar;
        }

        private void LimpiarCamposDeEmpleados()
        {
            txbApellidoEmpleado.Clear();
            txbNombreEmpleado.Clear();
            txbEmpleadosId.Text = "";
            txbPuestoEmpleado.Clear();
        }

        private void PonerBotonesEmpleadosEnEdicion(bool value)
        {
            btnCancelar.IsEnabled = value;
            btnEditarEmpleado.IsEnabled = !value;
            btnEliminarEmpleado.IsEnabled = !value;
            btnGuardarEmpleado.IsEnabled = value;
            btnAgregarEmpleado.IsEnabled = !value;
        }

        private void btnAgregarEmpleado_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeEmpleados();
            PonerBotonesEmpleadosEnEdicion(true);
            accionEmpleados = accion.Nuevo;
            HabilitarCajas(true);
            HabilitarBotones(false);
            esNuevo = true;
        }

        private void btnEditarEmpleado_Click(object sender, RoutedEventArgs e)
        {
            Empleado emp = dtgEmpleados.SelectedItem as Empleado;
            if (emp != null)
            {
                HabilitarCajas(true);
                txbEmpleadosId.Text = emp.Id;
                txbApellidoEmpleado.Text = emp.Apellidos;
                txbPuestoEmpleado.Text = emp.Area;
                txbNombreEmpleado.Text = emp.Nombre;
                accionEmpleados = accion.Editar;
                PonerBotonesEmpleadosEnEdicion(true);
            }
        }

        private void btnEliminarEmpleado_Click(object sender, RoutedEventArgs e)
        {
            Empleado emp = dtgEmpleados.SelectedItem as Empleado;
            if (emp != null)
            {
                if (MessageBox.Show("Realmente deseas eliminar este empleado?", "Farmacia", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorEmpleados.Eliminar(emp.Id))
                    {
                        MessageBox.Show("Empleado eliminado", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTablaEmpleados();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo eliminar el empleado", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
        }

        private void btnGuardarEmpleado_Click(object sender, RoutedEventArgs e)
        {
            if (accionEmpleados == accion.Nuevo)
            {
                Empleado emp = new Empleado()
                {
                    Nombre = txbNombreEmpleado.Text,
                    Apellidos = txbApellidoEmpleado.Text,
                    Area = txbPuestoEmpleado.Text
                };
                if (manejadorEmpleados.Agregar(emp))
                {
                    MessageBox.Show("Empleado agregado correctamente", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeEmpleados();
                    ActualizarTablaEmpleados();
                    HabilitarBotones(true);
                    HabilitarCajas(false);
                    PonerBotonesEmpleadosEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("El Empleado no se pudo agregar", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                Empleado emp = dtgEmpleados.SelectedItem as Empleado;
                emp.Apellidos = txbApellidoEmpleado.Text;
                emp.Area = txbPuestoEmpleado.Text;
                emp.Nombre = txbPuestoEmpleado.Text;
                if (manejadorEmpleados.Modificar(emp))
                {
                    MessageBox.Show("Empleado modificado correctamente", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeEmpleados();
                    HabilitarBotones(true);
                    HabilitarCajas(false);
                    ActualizarTablaEmpleados();
                    PonerBotonesEmpleadosEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("El Empleado no se pudo actualizar", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            HabilitarCajas(false);
            HabilitarBotones(true);
            LimpiarCamposDeEmpleados();
            PonerBotonesEmpleadosEnEdicion(false);
        }

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }    
    }
}
