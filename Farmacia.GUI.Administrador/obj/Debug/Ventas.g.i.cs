#pragma checksum "..\..\Ventas.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "7FF9925E74F5F0134BD061AB44ED24A184E6696F"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using Farmacia.GUI.Administrador;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Farmacia.GUI.Administrador
{


    /// <summary>
    /// Ventas
    /// </summary>
    public partial class Ventas : System.Windows.Window, System.Windows.Markup.IComponentConnector
    {

        private bool _contentLoaded;

        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent()
        {
            if (_contentLoaded)
            {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Farmacia.GUI.Administrador;component/ventas.xaml", System.UriKind.Relative);

#line 1 "..\..\Ventas.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);

#line default
#line hidden
        }

        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target)
        {
            this._contentLoaded = true;
        }

        internal System.Windows.Controls.Button btnCancelar;
        internal System.Windows.Controls.Button btnCerrar;
        internal System.Windows.Controls.ComboBox cmbCategoriaProducto;
        internal System.Windows.Controls.ComboBox cmbNombreProducto;
        internal System.Windows.Controls.Button btnAgregaVenta;
        internal System.Windows.Controls.Button btnEliminaVenta;
        internal System.Windows.Controls.Button btnGuardarVenta;
        internal System.Windows.Controls.DataGrid dtgVentas;
        internal System.Windows.Controls.TextBox txbCantidad;
        internal System.Windows.Controls.TextBlock txbVentasId;
        internal System.Windows.Controls.ComboBox cmbEmpleado;
        internal System.Windows.Controls.ComboBox cmbCliente;
        internal System.Windows.Controls.Button btnEditarVenta;
    }
}

