﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Farmacia.BIZ;
using Farmacia.COMMON.Interfaces;
using Farmacia.DAL;
using LiteDB;
using Farmacia.COMMON.Entidades;
using System;

namespace Farmacia.GUI.Administrador
{
    /// <summary>
    /// Lógica de interacción para Venta.xaml
    /// </summary>
    public partial class Ventas : Window
    {
        enum accion
        {
            Nuevo,
            Editar
        }
        IManejadorVentas manejadorVentas;
        IManejadorClientes manejadorClientes;
        IManejadorProductos manejadorProducto;
        IManejadorEmpleados manejadorEmpleado;
        Ventas ventas;
        accion accionVentas;
        public Ventas()
        {
            InitializeComponent();
            manejadorVentas = new ManejadorDeVentas(new RepositorioDeVentas());
            manejadorClientes = new ManejadorClientes(new RepositorioDeClientes());
            manejadorProducto = new ManejadorProductos(new RepositorioDeProductos());
            manejadorEmpleado = new ManejadorEmpleados(new RepositorioDeEmpleados());
            HabilitarCajas(false);
            HabilitarBotones(true);
            actualizarTabla();
        }

        private void actualizarTabla()
        {
            dtgTicket.ItemsSource = null;
            dtgTicket.ItemsSource = manejadorVentas.Listar;
        }

        private void btnRegresar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void HabilitarCajas(bool habilitadas)
        {
            cmbNombre.ItemsSource = null;
            cmbNombre.ItemsSource = manejadorClientes.Listar;
            cmbProducto.ItemsSource = null;
            cmbProducto.ItemsSource = manejadorProducto.Listar;
            cmbEmpleado.ItemsSource = null;
            cmbEmpleado.ItemsSource = manejadorEmpleado.Listar;
            txtCantidad.Clear();
            cmbNombre.IsEnabled = habilitadas;
            cmbProducto.IsEnabled = habilitadas;
            cmbEmpleado.IsEnabled = habilitadas;
            txtCantidad.IsEnabled = habilitadas;
        }

        private void HabilitarBotones(bool habilitados)
        {
            btnNuevo.IsEnabled = habilitados;
            btnEditar.IsEnabled = habilitados;
            btnEliminar.IsEnabled = habilitados;
            btnGuardar.IsEnabled = !habilitados;
            btnCancelar.IsEnabled = !habilitados;
        }
        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            HabilitarCajas(true);
            HabilitarBotones(false);
            accionVentas = accion.Nuevo;

        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            Ventasst pro = dtgTicket.SelectedItem as Ventasst;
            if (pro != null)
            {
                HabilitarCajas(true);
                cmbNombre.Text = pro.Cliente;
                cmbProducto.Text = pro.Productos;
                cmbEmpleado.Text = pro.Encargado;
                txtCantidad.Text = pro.cantidad;
                dtpFecha.SelectedDate = pro.FechaHora;
                accionVentas = accion.Editar;
                HabilitarBotones(false);
            }
            else
            {
                MessageBox.Show("Seleccione la venta que desea editar", "Productos", MessageBoxButton.OK, MessageBoxImage.Question);
            }
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (accionVentas == accion.Nuevo)
            {
                Ventasst pro = new Ventasst()
                {
                    Cliente = cmbNombre.Text,
                    FechaHora = dtpFecha.SelectedDate.Value,
                    Productos = cmbProducto.Text,
                    Encargado = cmbEmpleado.Text,
                    cantidad = txtCantidad.Text,
                    Total = txtCantidad.Text,
                };
                if (manejadorVentas.Agregar(pro))
                {
                    MessageBox.Show("La venta se ha realizado exitosamente", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                    actualizarTabla();
                    HabilitarBotones(true);
                    HabilitarCajas(false);
                }
                else
                {
                    MessageBox.Show("La venta no se ha podido realizar", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                Ventasst pro = dtgTicket.SelectedItem as Ventasst;         
                pro.cantidad = txtCantidad.Text;
                pro.Total = txtCantidad.Text;
                if (manejadorVentas.Modificar(pro))
                {
                    MessageBox.Show("Producto modificado correctamente", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                    actualizarTabla();
                    HabilitarBotones(true);
                    HabilitarCajas(false);
                }
                else
                {
                    MessageBox.Show("El producto no se pudo actualizar", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            HabilitarCajas(false);
            HabilitarBotones(true);
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            Ventasst pro = dtgTicket.SelectedItem as Ventasst;
            if (pro != null)
            {
                if (MessageBox.Show("Realmente deseas eliminar esta venta?", "Farmacia", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorVentas.Eliminar(pro.Id))
                    {
                        MessageBox.Show("La venta ha sido eliminado correctamente", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                        actualizarTabla();
                    }
                    else
                    {
                        MessageBox.Show("La venta no se pudo eliminar", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Error);
                    }

                }
            }
        }
    }
}